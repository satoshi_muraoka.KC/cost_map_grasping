'''コストマップを使って最適な把持位置を探索するスクリプト。
'''
import cv2
import kornia
import numpy as np
import structlog
import time
# import timm
import torch
# import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# import torchvision

# from torchinfo import summary
# from pyntcloud import PyntCloud


# 対象物上端から把持位置までの差し込み深さ＋バッファ [m]
depth_buffer = 0.06
# 対象物周辺を切り取るためにバウンディングボックスに加えるバッファ [pixel]
image_crop_buffer = 50


class CostMapGrasp():
    def __init__(self, logger: structlog.stdlib.BoundLogger, save_dir: str) -> None:
        self._logger = logger
        self.save_dir = save_dir
        self.optimization_time = 0

        # 描画用画像
        self.object_depth_image = np.array([])
        self.limited_depth_image = np.array([])
        self.color_image = np.array([])
        self.depth_image = np.array([])
        self.mask_image = np.array([])
        self.cost_map_cog = np.array([])
        self.cost_map_dist = np.array([])
        self.cost_map_all = np.array([])
        self.costmap_and_warpedtemplate_image = np.array([])
        self.mask_and_warpedtemplate_image = np.array([])
        self.color_and_warpedtemplate_image = np.array([])
        self.depth_and_warpedtemplate_image = np.array([])
        self.intermediates = []


    def get_optimization_time(self) -> np.ndarray:
        return self.optimization_time


    def get_object_depth_image(self) -> np.ndarray:
        return self.object_depth_image


    def get_limited_depth_image(self) -> np.ndarray:
        return self.limited_depth_image


    def get_color_image(self) -> np.ndarray:
        return self.color_image


    def get_depth_image(self) -> np.ndarray:
        return self.depth_image


    def get_mask_image(self) -> np.ndarray:
        return self.mask_image


    def get_cost_map_cog(self) -> np.ndarray:
        return self.cost_map_cog


    def get_cost_map_dist(self) -> np.ndarray:
        return self.cost_map_dist


    def get_cost_map_all(self) -> np.ndarray:
        return self.cost_map_all


    def get_costmap_and_warpedtemplate_image(self) -> np.ndarray:
        return self.costmap_and_warpedtemplate_image


    def get_mask_and_warpedtemplate_image(self) -> np.ndarray:
        return self.mask_and_warpedtemplate_image
    

    def get_color_and_warpedtemplate_image(self) -> np.ndarray:
        return self.color_and_warpedtemplate_image


    def get_depth_and_warpedtemplate_image(self) -> np.ndarray:
        return self.depth_and_warpedtemplate_image


    def get_intermediates(self) -> list:
        return self.intermediates


    def edit_dist_cost_map(self, depth, mask, cost_map_dist, cost_map_cog, depth_scale) -> np.ndarray:
        '''距離由来のコストマップに深度データを加えて新たなコストマップを作成する。
        '''
        # depth + depth_buffer（ハンドの差し込み深さ）までの depth だけを取得して dist のコストマップに追加する。

        # 対象物の depth のみ取得する。
        object_depth = np.multiply(depth, mask)
        self.object_depth_image = object_depth / np.max(object_depth) * 255
    
        # 対象物の depth の最頻値を取得する。
        unique, freq = np.unique(object_depth[object_depth != 0], return_counts=True)
        object_depth_mode = unique[np.argmax(freq)]
    
        # depth_scale をもとに採用する depth の上限値を計算する。
        depth_limit_max = object_depth_mode + depth_buffer / depth_scale

        # depth_limit_max を超える depth は 0 に変換する。
        limited_depth = np.where(depth > depth_limit_max, 0, depth)
        self.limited_depth_image = limited_depth / np.max(limited_depth) * 255
        
        # depth を cost_map_dist レンジにスケーリングして cost_map_dist に加算する。
        scaled_depth = (np.max(cost_map_dist) - np.min(cost_map_dist)) * (limited_depth - np.min(limited_depth)) / (np.max(limited_depth) - np.min(limited_depth))
        sum_dist_depth = cost_map_dist + scaled_depth
        
        # cost_map_dist と depth の和を cost_map_cog レンジにスケーリングする。
        # NOTE:cost_map_dist と depth を単純に加算しただけでは cost_map_cog に対して値が大きくなってしまうため。 
        ret = (np.max(cost_map_cog) - np.min(cost_map_cog)) * (sum_dist_depth - np.min(sum_dist_depth)) / (np.max(sum_dist_depth) - np.min(sum_dist_depth))
    
        return ret
    

    def estimate(self, color, depth, template, bbox, mask, depth_scale) -> list:
        '''把持位置を推定する。
        '''
        
        color_height, color_width, _ = color.shape
    
        x1, y1, x2, y2 = map(int, bbox)

        # 対象物周辺の切り取り範囲を計算する。
        x1 = x1 - image_crop_buffer
        y1 = y1 - image_crop_buffer
        x2 = x2 + image_crop_buffer
        y2 = y2 + image_crop_buffer
        # 切り取り範囲が画像からはみ出す場合の対応。
        x1 = 0 if x1 < 0 else x1
        y1 = 0 if y1 < 0 else y1
        x2 = color_width if x2 > color_width else x2
        y2 = color_height if y2 > color_height else y2
    
        color_image = color[y1:y2, x1:x2].copy()
        self.color_image = color_image
        height, width, _ = color_image.shape

        depth_image = depth[y1:y2, x1:x2].copy()
        self.depth_image = depth_image / np.max(depth_image) * 255

        grasping_template = template.copy()

        mask_image = mask[y1:y2, x1:x2].copy()
        mask_image_255 = mask_image * 255
        self.mask_image = mask_image_255
        mask_image_255_3c = np.stack((mask_image_255, mask_image_255, mask_image_255), axis=-1)
    
        # 対象物のマスク重心を最小とし、重心からのユークリッド距離が大きいほど高くなるコストマップ（cog）を作成する。
        # mask --> cog --> distance from cog
        # マスクの重心（x, y) = (m10/m00, m01/m00）を計算する。
        momentum = cv2.moments(mask_image, False)
        x, y = momentum["m10"] / momentum["m00"], momentum["m01"] / momentum["m00"]
        # x 座標のアドレスを要素とする配列を作成する。
        xarr = np.reshape(np.array([x for x in range(color_image.shape[1])] * color_image.shape[0]), mask_image.shape)
        # xarr = [[
        #     0 1 2 ..
        #     0 1 2 ..
        #     0 1 2 ..
        #     :
        # ]]
        # y 座標のアドレスを要素とする配列を作成する。
        yarr = np.repeat(
            np.reshape([y for y in range(color_image.shape[0])], (-1, 1)), color_image.shape[1], axis=1
        )
        # yarr = [[
        #     0 0 0 ..
        #     1 1 1 ..
        #     2 2 2 ..
        #     :
        # ]]
        # 各アドレスの重心からのユークリッド距離^2（分散）を計算する。
        cost_map_cog = np.square(xarr - x) + np.square(yarr - y)
        # 各アドレスの重心からの√分散（標準偏差）を計算する。
        cost_map_cog = np.sqrt(cost_map_cog)
        self.cost_map_cog = cost_map_cog
        
        # 物体からの距離マップを作成する。
        # mask --> distance from edge
        # mask を 0-> 255, 255 -> 0 に反転する。
        mask_ = 255 - mask_image_255
        # 白画素に対して、5*5 のフィルタを使用して最近傍黒画素までのマンハッタン距離を計算する。
        dist1 = cv2.distanceTransform(mask_image_255, cv2.DIST_L1, 5)  # 物体画素の背景までの最短マンハッタン距離
        dist2 = cv2.distanceTransform(mask_, cv2.DIST_L1, 5)  # 背景画素の物体までの最短マンハッタン距離
        # 物体画素が持つ背景までの距離（dist1） << 背景画素が持つ物体までの距離（dist2）のため、
        # mask * 0.5 のオフセットを足すことで全体のバランスをとる。
        cost_map_dist = mask_image_255 * 0.5 + dist1 + dist2
        self.cost_map_dist = cost_map_dist
        
        # # cost_map_dist に depth 由来のコストマップを追加する。
        # cost_map_dist = self.edit_dist_cost_map(depth_image, mask_image, cost_map_dist, cost_map_cog, depth_scale)
        
        # cost_map = dist / dist / cog
        # cost_map_dist と cost_map_cog を (height, width, 1) の三次元配列に変換する。
        cost_map_dist = np.reshape(cost_map_dist, (height, width, 1))
        cost_map_cog = np.reshape(cost_map_cog, (height, width, 1))
        # cost_map_dist, cost_map_dist, cost_map_cog を三次元方向に連結して cost_map を作成する。
        cost_map = np.concatenate([cost_map_dist, cost_map_dist, cost_map_cog], axis=2)
        # 描画用
        cost_map_gray_255 = np.squeeze(cost_map_dist + cost_map_cog / np.max(cost_map_dist + cost_map_cog) * 255)
        self.cost_map_all = cost_map_gray_255
     
        # array to torch.tensor
        # cost_map, grasping_template を PyTorch テンソルに変換し、次元を増やす。
        cost_map = torch.unsqueeze(kornia.image_to_tensor(cost_map).float(), dim=0)  # HWC --> NCHW
        template = torch.unsqueeze(kornia.image_to_tensor(grasping_template).float(), dim=0)
        
        # 出力画像サイズ：height * width、補完モード：バイリニア補完でホモグラフィ―変換のオブジェクトを作成する。
        warper = kornia.geometry.HomographyWarper(height=height, width=width, mode="bilinear")
        # 変換行列を生成するためのテンソルを作成する。勾配計算あり。
        M = torch.tensor([[1.0, 0.0, 0.0, 0.0]], requires_grad=True)  # [scale, theta, tx, ty]
        tensor_0 = torch.zeros(1)
        tensor_1 = torch.ones(1)
        params = [M]
        # 最適化オブジェクトを作成。最適化アルゴリズム：Adam、学習率：0.02。
        optimizer = optim.Adam(params, lr=0.02)
        # Mを最適化する繰り返し計算。
        start_optimization = time.time()  # 開始時間
        for iter in range(2000):
            optimizer.zero_grad()
            P = torch.stack(
                [
                    torch.stack(
                        [M[:, 0] * torch.cos(M[:, 1]), -M[:, 0] * torch.sin(M[:, 1]), M[:, 2]]
                    ),
                    torch.stack(
                        # [M[:, 0] * torch.sin(M[:, 1]), M[:, 0] * torch.cos(M[:, 1]), M[:, 3]]
                        [torch.sin(M[:, 1]), torch.cos(M[:, 1]), M[:, 3]]
                    ),
                    torch.stack([tensor_0, tensor_0, tensor_1]),
                ]
            ).reshape(-1, 3, 3)
            # P = [
            #     [cos  sin  tx]
            #     [sin -cos  ty]
            #     [  0    0   1]
            # ]
            # テンプレート画像を変換行列で変換する。
            warp = warper(template, P)
            # 変換した画像とコストマップの要素ごとに乗算して現時点でのコストを得る。
            evaluation = warp * cost_map
            # 変換した画像と、同形状の値 0 の画像から平均二乗誤差を計算する。
            loss = F.mse_loss(evaluation, torch.zeros_like(evaluation), reduction="mean")
            # 損失関数の勾配を計算する。
            loss.backward()
            # 勾配降下処理を実行してMを更新する。
            optimizer.step()
        
            # 勾配を除外した Tensor を CPU メモリに移動し、numpy に変換する。
            warped_template = warp.detach().cpu().numpy()
            # warped_template から一次元の要素を削除し、1 -> 0, 2 -> 1, 0 -> 2 に要素の順番を入れ替える。
            warped_template = np.transpose(np.squeeze(warped_template), [1, 2, 0])
            if 0 == iter % 100:
                self._logger.info(
                    "{}: scale = {:03f}, theta = {:03f}, tx = {:03f}, ty = {:03f}".format(iter, M[0][0], M[0][1], M[0][2], M[0][3])
                )
                # 描画用
                # メモリ削減のためテンプレート画像をエンコードして保持する。
                is_success, buff = cv2.imencode(".png", warped_template, [int(cv2.IMWRITE_PNG_COMPRESSION), 5])                
                if is_success:
                    self.intermediates.append(np.array(buff).tostring())

        self.optimization_time = time.time() - start_optimization  # 経過時間

        matP = P.detach().cpu().numpy()
        matP = np.reshape(matP, (3, 3))
        self._logger.info(f"matP = {matP}")
        self._logger.info(f"inverse matP = {np.linalg.inv(matP)}")

        warped_template_position = (np.any(warped_template != [0, 0, 0], axis=-1) * 255).astype(np.uint8)

        # 描画用
        costmap_and_warpedtemplate = np.stack((cost_map_gray_255, cost_map_gray_255, cost_map_gray_255), axis=-1).copy()
        costmap_and_warpedtemplate[warped_template_position != 0] = warped_template[warped_template_position != 0]
        self.costmap_and_warpedtemplate_image = costmap_and_warpedtemplate
        # 描画用
        mask_and_warpedtemplate = mask_image_255_3c.copy()
        mask_and_warpedtemplate[warped_template_position != 0] = warped_template[warped_template_position != 0]
        self.mask_and_warpedtemplate_image = mask_and_warpedtemplate
        # 描画用
        color_and_warpedtemplate = color_image.copy()
        color_and_warpedtemplate[warped_template_position != 0] = warped_template[warped_template_position != 0]
        self.color_and_warpedtemplate_image = color_and_warpedtemplate
        # 描画用
        depth_and_warpedtemplate = np.stack((self.depth_image, self.depth_image, self.depth_image), axis=-1).copy()
        depth_and_warpedtemplate[warped_template_position != 0] = warped_template[warped_template_position != 0]
        self.depth_and_warpedtemplate_image = depth_and_warpedtemplate

        # template の各色成分を取り出してマスク画像に変換する。
        # NOTE: 閾値の妥当性？
        # ホモグラフィ―変換したテンプレートの Green と Blue 成分に 0-50 程度の Red 成分が残る（なぜ？）。
        # これを切り捨てるための閾値は 0-255 のどの値が妥当か。
        warped_template = (warped_template * 255 / np.max(warped_template)).astype(np.uint8)
        # blue
        template_blue = warped_template[:, :, 0]
        template_blue = np.where(template_blue > 128, template_blue, 0)
        template_blue_mask = np.where(template_blue > 0, 1, 0).astype(np.uint8)
        # green
        template_green = warped_template[:, :, 1]
        template_green = np.where(template_green > 128, template_green, 0)
        template_green_mask = np.where(template_green > 0, 1, 0).astype(np.uint8)
        # red
        template_red = warped_template[:, :, 2]
        template_red = np.where(template_red > 128, template_red, 0)
        template_red_mask = np.where(template_red > 0, 1, 0).astype(np.uint8)

        # template 色成分マスクの重心を計算
        # NOTE: ハンド位置の妥当性？
        # Green と Blue 成分それぞれの重心をハンドの位置とするのは妥当か。
        # もし Green と Blue が太線のままであれば、対象物表面から離れた位置をハンドの位置とする可能性がある。
        momentum_blue = cv2.moments(template_blue_mask, False)
        cog_blue = np.array(list(map(int, (
            momentum_blue["m10"] / momentum_blue["m00"],
            momentum_blue["m01"] / momentum_blue["m00"]
        ))))
        momentum_green = cv2.moments(template_green_mask * 255, False)
        cog_green = np.array(list(map(int, (
            momentum_green["m10"] / momentum_green["m00"],
            momentum_green["m01"] / momentum_green["m00"]
        ))))
        momentum_red = cv2.moments(template_red_mask * 255, False)
        cog_red = np.array(list(map(int, (
            momentum_red["m10"] / momentum_red["m00"],
            momentum_red["m01"] / momentum_red["m00"]
        ))))
 
        return_list = [
            cog_red[0] + x1,  # tool center point x [pixel]
            cog_red[1] + y1,  # tool center point y [pixel]
            M[:,1],  # angle [radian]
            np.linalg.norm(cog_blue - cog_green)  # gripper open width [pixel]
        ]

        return return_list
