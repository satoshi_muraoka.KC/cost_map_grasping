'''cost_map_graspにテストデータを渡し、結果を取得するスクリプト。
'''

import argparse
import cv2
import glob
import json
import numpy as np
import os
import pytz
import structlog
import sys
import time
from abc import ABCMeta, abstractmethod
from datetime import datetime
from pycocotools import mask
from typing import Optional, TypedDict

import cost_map_grasp
from pylog import get_logger


class Image(TypedDict):
    data: np.ndarray
    format: int

Template = Image

class Depth(TypedDict):
    data: np.ndarray
    format: int


class Mask(TypedDict):
    size: list[float]
    counts: str


class Object(TypedDict):
    bbox: list[float]
    mask: Mask


class Position(TypedDict):
    x: int
    y: int


class Angle(TypedDict):
    theta: float


class Width(TypedDict):
    width: float


class Result(TypedDict):
    position: Position
    angle: Angle
    open_width: Width


class Executer(metaclass=ABCMeta):

    @abstractmethod
    def estimate(
        self,
        image: Image,
        depth: Depth,
        template: Template,
        obj: Object,
        depth_scale: float,
    ) -> Result:
        pass


class KCRExecuter(Executer):

    def __init__(self, logger: structlog.stdlib.BoundLogger, save_dir: str) -> None:
        self._logger = logger
        self.save_dir = save_dir

        estimator = cost_map_grasp.CostMapGrasp(logger, save_dir)
        self._estimator = estimator.estimate
        self._get_optimization_time = estimator.get_optimization_time
        self._get_object_depth_image = estimator.get_object_depth_image
        self._get_limited_depth_image = estimator.get_limited_depth_image
        self._get_color_image = estimator.get_color_image
        self._get_depth_image = estimator.get_depth_image
        self._get_mask_image = estimator.get_mask_image
        self._get_cost_map_cog = estimator.get_cost_map_cog
        self._get_cost_map_dist = estimator.get_cost_map_dist
        self._get_cost_map_all = estimator.get_cost_map_all
        self._get_costmap_and_warpedtemplate_image = estimator.get_costmap_and_warpedtemplate_image
        self._get_mask_and_warpedtemplate_image = estimator.get_mask_and_warpedtemplate_image
        self._get_color_and_warpedtemplate_image = estimator.get_color_and_warpedtemplate_image
        self._get_depth_and_warpedtemplate_image = estimator.get_depth_and_warpedtemplate_image
        self._get_intermediates = estimator.get_intermediates


    def estimate(
        self,
        image: Image,
        depth: Depth,
        template: Template,
        obj: Object,
        depth_scale: float,
    ) -> Optional[Result]:

        start = time.time()

        output = self._estimator(
            color=image["data"],
            depth=depth["data"],
            template=template["data"],
            bbox=obj["bbox"],
            mask=mask.decode({"counts": obj["mask"]["counts"], "size": [int(i) for i in obj["mask"]["size"]]}),
            depth_scale=depth_scale,
        )
        self._logger.debug('estimation was finished', output_data=output)

        if isinstance(output, int) and output == 1:
            return None

        processed = time.time()

        result = Result(
            pos=Position(x=output[0], y=output[1]),
            angle=Angle(theat=output[2]),
            gripper_width=Width(width=output[3])

        )
        formatted = time.time()

        self._logger.info(
            'estimate method was finished',
            time={
                "total": (formatted - start),
                "process": (processed - start),
                "format": (formatted - processed),
                "optimaize": self._get_optimization_time()
            },
        )

        return result


    def save_intermediates(self, mode="cost") -> None:
        if mode not in ["color", "depth", "mask", "cost"]:
            self._logger.info(
                f"mode = {mode} is an invalid value. Convert to mode = cost."
            )
            mode = "cost"

        if mode == "color":
            base_image = self._get_color_image()
        elif mode == "depth":
            depth_image = self._get_depth_image()
            base_image = np.stack((depth_image, depth_image, depth_image), axis=-1)
        elif mode == "cost":
            cost_map_all = self._get_cost_map_all()
            base_image = np.stack((cost_map_all, cost_map_all, cost_map_all), axis=-1)
        elif mode == "mask":
            mask_image = self._get_mask_image()
            base_image = np.stack((mask_image, mask_image, mask_image), axis=-1)
        
        for idx, buff in enumerate(self._get_intermediates(), 1):
            nparr = np.frombuffer(buff, np.uint8)
            warped_template = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
            base_image_copy = base_image.copy()
            intermediate_mask = (np.any(warped_template != [0, 0, 0], axis=-1) * 255).astype(np.uint8)
            base_image_copy[intermediate_mask != 0] = warped_template[intermediate_mask != 0]
            cv2.imwrite(self.save_dir + "/intermediate_{:05d}.png".format(idx), base_image_copy)


    # template が最適化される様子を動画で保存する。
    def save_transition_video(self, mode="cost") -> None:
        filenames = sorted(glob.glob(self.save_dir + "/intermediate_*.png"))
        # intermediate_*.png が無い場合は作成する。
        if not filenames:
            self.save_intermediates(mode)
            filenames = sorted(glob.glob(self.save_dir + "/intermediate_*.png"))

        # 最初の画像からフレームサイズを取得する。
        img = cv2.imread(filenames[0])
        height, width, _ = img.shape
        
        # ビデオライターを定義する。mp4用のコーデック：mp4v、フレームレート：10.0。
        video = cv2.VideoWriter(self.save_dir + "/output.mp4", cv2.VideoWriter_fourcc(*"mp4v"), 10.0, (width,height))
        
        # 全画像をビデオのフレームとして書き込む。
        for filename in filenames:
            img = cv2.imread(filename)
            video.write(img)
        
        # 書き込み操作終了。
        video.release()
        self._logger.info(
            'creating video was finished'
        )


    # 最適化された template を重ねた画像を保存する。
    def save_result_image(self) -> None:
        image = self._get_color_and_warpedtemplate_image()
        cv2.imwrite(self.save_dir + "/result_color.png", image)
        image = self._get_depth_and_warpedtemplate_image()
        cv2.imwrite(self.save_dir + "/result_depth.png", image)
        image = self._get_mask_and_warpedtemplate_image()
        cv2.imwrite(self.save_dir + "/result_mask.png", image)
        image = self._get_costmap_and_warpedtemplate_image()
        cv2.imwrite(self.save_dir + "/result_costmap.png", image)


def get_args(args: list[str]) -> argparse.Namespace:
    defaultdir = 'testing/data'
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_image', type=str, help='input image file path', default=defaultdir + '/image.png')
    parser.add_argument('--input_image_format', choices=['png', 'bgr8'], help='input image file format (png or bgr8)', default='png')
    parser.add_argument('--input_depth', type=str, help='input depth file path', default=defaultdir + '/depth.bin')
    parser.add_argument('--input_depth_format', choices=['z16'], help='input depth file format', default='z16')
    parser.add_argument('--input_template', type=str, help='input template file path', default=defaultdir + '/template.png')
    parser.add_argument('--input_template_format', choices=['png', 'bgr8'], help='input template file format (png or bgr8)', default='png')
    parser.add_argument('--input_camconfig', type=str, help='input camera config file (json) path', default=defaultdir + '/camconfig.json')
    parser.add_argument('--input_obj', type=str, help='input box file (json) path', default=defaultdir + '/object.json')

    tokyo_tz = pytz.timezone('Asia/Tokyo')
    save_dir = "output/" + datetime.now(tokyo_tz).strftime('%Y%m%d_%H%M%S_%f')
    parser.add_argument('--save_dir', help="save image directory", type=str, default=save_dir)

    return parser.parse_args(args)


def main(logger: structlog.stdlib.BoundLogger, executer: Executer, args: argparse.Namespace) -> Optional[Result]:

    logger.info(
        'start script preparation.',
        input_image=args.input_image,
        input_image_format=args.input_image_format,
        input_depth=args.input_depth,
        input_depth_format=args.input_depth_format,
        input_template=args.input_template,
        input_template_format=args.input_template_format,
        input_camconfig=args.input_camconfig,
        input_obj=args.input_obj,
        save_dir=args.save_dir,
    )

    # image
    if not os.path.isfile(args.input_image):
        raise ValueError(f'{args.input_image} does not exist or is not a file.')
    if args.input_image_format == 'bgr8':
        image_data = np.fromfile(args.input_image, np.uint8).reshape(720, 1280, 3)
    elif args.input_image_format == 'png':
        image_data = cv2.imread(args.input_image)  # type: ignore
    else:
        raise ValueError(f'{args.input_image_format} does not support.')

    # depth
    if not os.path.isfile(args.input_depth):
        raise ValueError(f'{args.input_depth} does not exist or is not a file.')
    if args.input_depth_format == 'z16':
        depth_data = np.fromfile(args.input_depth, np.uint16).reshape(720, 1280)
    else:
        raise ValueError(f'{args.input_depth_format} does not support.')

    # template
    if not os.path.isfile(args.input_template):
        raise ValueError(f'{args.input_template} does not exist or is not a file.')
    if args.input_template_format == 'bgr8':
        template_data = np.fromfile(args.input_template, np.uint8).reshape(720, 1280, 3)
    elif args.input_template_format == 'png':
        template_data = cv2.imread(args.input_template)  # type: ignore
    else:
        raise ValueError(f'{args.input_template_format} does not support.')

    # camera configuration
    if not os.path.isfile(args.input_camconfig):
        raise ValueError(f'{args.input_camconfig} does not exist or is not a file.')
    with open(args.input_camconfig) as f:
        input_camconfig = json.load(f)

    # detected object
    if not os.path.isfile(args.input_obj):
        raise ValueError(f'{args.input_obj} does not exist or is not a file.')
    with open(args.input_obj) as f:
        input_obj = json.load(f)

    result = executer.estimate(
        image=Image(
            data=image_data,
            format=args.input_image_format,
        ),
        depth=Depth(
            data=depth_data,
            format=args.input_depth_format,
        ),
        template=Template(
            data=template_data,
            format=args.input_template_format,
        ),
        obj=Object(
            bbox=[float(i) for i in input_obj[0]["bbox"]],
            mask=Mask(counts=input_obj[0]["mask"]["counts"], size=input_obj[0]["mask"]["size"])
        ),
        depth_scale=input_camconfig["depth"]["scale"]
    )

    executer.save_transition_video("cost")
    executer.save_result_image()


if __name__ == '__main__':
    args = get_args(sys.argv[1:])

    if args.save_dir is not None:
        try:
            os.makedirs(args.save_dir, exist_ok=True)
            print('Images will be saved in:', args.save_dir)
        except OSError as err:
            print("The directory could not be created. Error: ", err)

    logger = get_logger(__name__, bind_id=False)

    executer = KCRExecuter(logger, args.save_dir)

    main(logger, executer, args)
