import numpy as np
import cv2
import json
import os


crop_area = [718, 150, 718 + 282, 150 + 347]

color_pth = "kitting_test_color_0.png"
object_mask_pth = "kitting_test_object_mask_0.png"
partition_mask_pth = "kitting_test_partition_mask_0.png"
depth_pth = "kitting_test_depth_0.bin"
info_pth = "kitting_test_info_0.json"

if os.path.exists(color_pth):
    color = cv2.imread(color_pth)
    cv2.imwrite("kitting_test_color.png", color[crop_area[1]:crop_area[3], crop_area[0]:crop_area[2]])

if os.path.exists(object_mask_pth):
    object_mask = cv2.imread(object_mask_pth)
    cv2.imwrite("kitting_test_object_mask.png", object_mask[crop_area[1]:crop_area[3], crop_area[0]:crop_area[2]])

if os.path.exists(partition_mask_pth):
    partition_mask = cv2.imread(partition_mask_pth)
    cv2.imwrite("kitting_test_partition_mask.png", partition_mask[crop_area[1]:crop_area[3], crop_area[0]:crop_area[2]])

if os.path.exists(object_mask_pth) and os.path.exists(partition_mask_pth):
    mask = object_mask + partition_mask
    cv2.imwrite("kitting_test_mask.png", mask[crop_area[1]:crop_area[3], crop_area[0]:crop_area[2]])

if os.path.exists(info_pth):
    with open(info_pth, "r") as jf:
        info = json.load(jf)
    depth_width = info["depth"]["width"]
    depth_height = info["depth"]["height"]
    depth_format = info["depth"]["format"]
    if depth_format == "Z16":
        depth_dtype = np.uint16

    if os.path.exists(depth_pth):
        with open(depth_pth, "rb") as f:
            b_depth = f.read()
        depth = np.frombuffer(b_depth, dtype=depth_dtype).reshape((depth_height, depth_width))
        cropped_depth = depth[crop_area[1]:crop_area[3], crop_area[0]:crop_area[2]]
        np.save('kitting_test_depth.npy', cropped_depth)
        cropped_depth.tofile('kitting_test_depth.bin')  # Save as .bin format
