# cost_map_grasping

コストマップをもとに把持位置を推定するアルゴリズム。

## Usage
### Git リポジトリのクローン
```bash
git clone git@gitlab.com:satoshi_muraoka.KC/cost_map_grasping.git
```
または
```bash
git clone https://gitlab.com/satoshi_muraoka.KC/cost_map_grasping.git
```

### ディレクトリ移動
```bash
cd cost_map_grasping
```

### ビルドの実行
```bash
./docker/build_image.sh
```

### コンテナの起動とログイン
```bash
./docker/run_exec_container.sh
```

### テストの実行
```bash
python test.py
```

実行結果は output/[実行日時] に保存される。