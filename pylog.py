'''
Title                            : Logger in Python
Application                      : Used to set custom logging system using logging module
Author                           : Tata Elxsi Development Team
Description                      : This script is used to stream logs to "stdout" and "stderr"
'''

import logging
import os
import sys
from distutils.util import strtobool

import structlog
from structlog.dev import ConsoleRenderer
from structlog.processors import JSONRenderer

# Here the string is converted to Boolean
PRODUCTION = strtobool(os.environ.get("PRODUCTION", 'True'))

structlog.configure(
    processors=[
        structlog.contextvars.merge_contextvars,
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.dev.set_exc_info,
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S", utc=False),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.processors.EventRenamer(to='message'),
        structlog.processors.CallsiteParameterAdder(
            [
                structlog.processors.CallsiteParameter.PATHNAME,
                structlog.processors.CallsiteParameter.FUNC_NAME,
                structlog.processors.CallsiteParameter.LINENO,
            ]
        ),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ] if PRODUCTION else [
        structlog.contextvars.merge_contextvars,
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.dev.set_exc_info,
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S", utc=False),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setFormatter(structlog.stdlib.ProcessorFormatter(
    processor=JSONRenderer() if PRODUCTION else ConsoleRenderer(),
))
handler_stdout.addFilter(lambda record: record.levelno <= logging.WARNING)

handler_stderr = logging.StreamHandler(sys.stderr)
handler_stderr.setFormatter(structlog.stdlib.ProcessorFormatter(
    processor=JSONRenderer() if PRODUCTION else ConsoleRenderer(),
))
handler_stderr.addFilter(lambda record: record.levelno > logging.WARNING)

root_logger = logging.getLogger()
root_logger.addHandler(handler_stdout)
root_logger.addHandler(handler_stderr)
root_logger.setLevel(logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO")))


def get_logger(name: str = __name__, bind_id: bool = PRODUCTION > 0) -> structlog.stdlib.BoundLogger:
    logger = structlog.get_logger(name)

    return logger.bind(
        robot_id=os.environ.get("ROBOT_ID", "unknown"),
        version_id=os.environ.get("VERSION_ID", "unknown"),
        library_id=os.environ.get("LIBRARY_ID", "unknown"),
        artifact_id=os.environ.get("ARTIFACT_ID", "unknown"),
    ) if bind_id is True else logger
